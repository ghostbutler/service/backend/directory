package service

import (
	"bytes"
	"encoding/json"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

// remote controller device
type RemoteControllerDevice struct {
	// name
	Name string

	// Controller
	Controller *RemoteController
}

// a controller which contact us is saved in this struct
// directory will then ask it its capabilities and which
// devices it controls
type RemoteController struct {
	// address
	Address string

	// name
	Name string

	// type
	ServiceType common.ServiceType

	// capabilities
	CapabilityList []device.CapabilityType

	// devices associated to capabilities
	DeviceLinkCapabilityList map[device.CapabilityType][]RemoteControllerDevice

	// last update
	lastUpdateTime time.Time

	// last contact time
	lastContactTime time.Time

	// metrics handler
	metric *Metric
}

// build remote controller
func BuildRemoteController(address string,
	serviceType common.ServiceType,
	serviceKey string,
	metric *Metric) *RemoteController {
	return &RemoteController{
		Address:                  address,
		DeviceLinkCapabilityList: make(map[device.CapabilityType][]RemoteControllerDevice),
		ServiceType:              serviceType,
		lastUpdateTime:           time.Now(),
		lastContactTime:          time.Now(),
		metric:                   metric,
		Name:                     serviceKey,
	}
}

// update
func (controller *RemoteController) Update(keyManager *keystore.KeyManager,
	httpClient *http.Client) {
	// request capabilities list
	request, _ := http.NewRequest("GET",
		"https://"+
			controller.Address+
			":"+
			strconv.Itoa(common.GhostService[controller.ServiceType].DefaultPort)+
			"/api/v1/capabilities",
		nil)

	// request a token
	if publicKey, err := keyManager.GetOneUseKey(); err == nil {
		// add ghost token header
		request.Header.Add(keystore.OneTimeKeyShareHeaderName,
			publicKey.PublicKey)

		// do the request
		if response, err := httpClient.Do(request); err == nil {
			// process response
			defer common.Close(response.Body)
			body, _ := ioutil.ReadAll(response.Body)
			rawCapabilityList := make([]string,
				0,
				1)
			if err := json.Unmarshal(body,
				&rawCapabilityList); err == nil {
				// build final list
				capabilityList := make([]device.CapabilityType,
					0,
					1)
				for _, rawCapability := range rawCapabilityList {
					// extract capability
					if capabilityType := device.FindCapabilityFromName(rawCapability); capabilityType != device.CapabilityTypes {
						capabilityList = append(capabilityList,
							capabilityType)
					}
				}

				// save
				controller.CapabilityList = capabilityList
			}
		}
	}

	if controller.CapabilityList != nil {
		// iterate controller capabilities
		for _, capability := range controller.CapabilityList {
			// request devices name for given capability
			request, _ := http.NewRequest("GET",
				"https://"+
					controller.Address+
					":"+
					strconv.Itoa(common.GhostService[controller.ServiceType].DefaultPort)+
					device.CapabilityTypeListURL[capability],
				nil)

			// add one time key
			if key, err := keyManager.GetOneUseKey(); err == nil {
				request.Header.Add(keystore.OneTimeKeyShareHeaderName,
					key.PublicKey)

				// send request
				if response, err := common.InsecureHTTPClient.Do(request); err == nil {
					// extract body
					body, _ := ioutil.ReadAll(response.Body)

					// close body
					common.Close(response.Body)

					// unmarshal
					deviceList := make([]string,
						0,
						1)
					if err := json.Unmarshal(body,
						&deviceList); err == nil {
						deviceListFinal := make([]RemoteControllerDevice,
							0,
							1)
						for _, name := range deviceList {
							deviceListFinal = append(deviceListFinal,
								RemoteControllerDevice{
									Name:       name,
									Controller: controller,
								})
						}
						// replace with new list
						controller.DeviceLinkCapabilityList[capability] = deviceListFinal
					}
				}
			}
		}
	}
}

// send http request
// url is of /api/v1/[...] form
func (controller *RemoteController) SendHTTPRequest(method string,
	url string,
	data []byte,
	keyManager *keystore.KeyManager) (*http.Response, error) {
	// get one time key
	if key, err := keyManager.GetOneUseKey(); err == nil {
		// build request
		request, _ := http.NewRequest(method,
			"https://"+
				controller.Address+
				":"+
				strconv.Itoa(common.GhostService[controller.ServiceType].DefaultPort)+
				url,
			bytes.NewBuffer(data))

		// add header
		request.Header.Add(keystore.OneTimeKeyShareHeaderName,
			key.PublicKey)
		request.Header.Add("Content-Type",
			"application/x-www-form-urlencoded")

		// send request
		return common.InsecureHTTPClient.Do(request)
	} else {
		return nil, err
	}
}

// is known capability for this controller?
func (controller *RemoteController) IsKnownCapability(capability device.CapabilityType) bool {
	// iterate capabilities
	for _, controllerCapability := range controller.CapabilityList {
		if controllerCapability == capability {
			return true
		}
	}
	return false
}
