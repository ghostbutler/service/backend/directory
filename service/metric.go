package service

import (
	"github.com/prometheus/client_golang/prometheus"
)

type Metric struct {
	// time to update active device
	UpdateDeviceTime *prometheus.SummaryVec

	// known devices count by type
	KnownDeviceCount *prometheus.GaugeVec

	// known controllers count by type
	KnownControllerCount *prometheus.GaugeVec
}

// build metrics handler
func BuildMetric() *Metric {
	metric := &Metric{}
	metric.UpdateDeviceTime = prometheus.NewSummaryVec(prometheus.SummaryOpts{
		Name: "directory_update_active_device_time",
		Help: "Delay to update active devices",
		ConstLabels: prometheus.Labels{
			"type": "ghost",
		},
	},
		[]string{})
	prometheus.MustRegister(metric.UpdateDeviceTime)

	metric.KnownDeviceCount = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "directory_known_device_count",
		Help: "Known device count by type",
		ConstLabels: prometheus.Labels{
			"type": "ghost",
		},
	},
		[]string{
			"device",
		},
	)
	prometheus.MustRegister(metric.KnownDeviceCount)

	metric.KnownControllerCount = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "directory_known_controller_count",
		Help: "Known controller count",
		ConstLabels: prometheus.Labels{
			"type": "ghost",
		},
	},
		[]string{},
	)
	prometheus.MustRegister(metric.KnownControllerCount)

	return metric
}
