package service

import (
	"fmt"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/ghostbutler/tool/service/device"
	"time"
)

const (
	ActiveSensorTimeFrame = 1
)

type Database struct {
	isRunning bool

	// mongo instance
	mongo *mgo.Session

	// database
	Database string

	// sensor collection
	SensorCollection string

	// remote controllers library
	remoteControllerLibrary *RemoteControllerLibrary

	// active sensors
	activeSensor []device.ActiveSensor

	// metrics handler
	metric *Metric
}

func BuildDatabase(configuration *Configuration,
	remoteControllerLibrary *RemoteControllerLibrary,
	metric *Metric) (*Database, error) {
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    configuration.MongoDB.Hostname,
		Timeout:  10 * time.Second,
		Database: configuration.MongoDB.Authentication.Database,
		Username: configuration.MongoDB.Authentication.UserName,
		Password: configuration.MongoDB.Authentication.Password,
	}
	if mongoSession, err := mgo.DialWithInfo(mongoDBDialInfo); err != nil {
		return nil, err
	} else {
		mongoSession.SetMode(mgo.Monotonic, true)
		mongoSession.SetSafe(&mgo.Safe{WMode: "majority"})
		database := &Database{
			isRunning:               true,
			metric:                  metric,
			mongo:                   mongoSession,
			Database:                configuration.MongoDB.Database,
			SensorCollection:        configuration.MongoDB.Collection.Sensor,
			remoteControllerLibrary: remoteControllerLibrary,
		}
		go database.updateThread()
		return database, nil
	}
}

func (database *Database) Close() {
	database.isRunning = false
	database.mongo.Close()
}

func (database *Database) isSensorPresent(name string,
	_type string,
	sensorData []*device.SensorData) bool {
	for _, sensorDataSample := range sensorData {
		//fmt.Println( sensorDataSample, "|", _type, name )
		if sensorDataSample.Type == _type &&
			sensorDataSample.Name == name {
			return true
		}
	}
	return false
}

func (database *Database) update() error {
	// extract device infos
	capabilityType := database.remoteControllerLibrary.ExtractKnownDeviceTypeList()
	deviceList := make(map[device.CapabilityType][]string)
	for _, capability := range capabilityType {
		deviceList[capability] = database.remoteControllerLibrary.ExtractControllerTypeList(device.CapabilityTypeName[capability])
	}

	// look for activity into database
	initialTime := time.Now()
	defer database.metric.UpdateDeviceTime.WithLabelValues().Observe(float64(time.Now().Sub(initialTime)))

	session := database.mongo.Copy()
	defer session.Close()
	collection := session.DB(database.Database).C(database.SensorCollection)
	var data []*device.SensorData
	if err := collection.Find(bson.M{
		"time": bson.M{
			"$gt": time.Now().Add(time.Second * ActiveSensorTimeFrame * -1),
		},
	},
	).All(&data); err == nil {
		for _, dataSample := range data {
			_ = dataSample.ParseData()
		}
		if len(data) > 0 {
			activeSensor := make([]device.ActiveSensor, 0, 1)
			for _, capability := range capabilityType {
				for _, name := range deviceList[capability] {
					if database.isSensorPresent(name,
						device.CapabilityTypeName[capability],
						data) {
						activeSensor = append(activeSensor,
							device.ActiveSensor{
								Name:           name,
								CapabilityType: device.CapabilityTypeName[capability],
							},
						)
					}
				}
			}
			database.activeSensor = activeSensor
		} else {
			database.activeSensor = nil
		}
	} else {
		return err
	}
	return nil
}

func (database *Database) updateThread() {
	for database.isRunning {
		if err := database.update(); err != nil {
			fmt.Println(err)
		}
		time.Sleep(time.Second)
	}
}
