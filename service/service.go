package service

// we do use this library to declare our endpoints
import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"github.com/streadway/amqp"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"net/http"
	"time"
)

// set here your service version
const (
	VersionMajor = 1
	VersionMinor = 0
)

// this is your service structure, which can contains more then the simple common.Service instance
type Service struct {
	// service
	service *common.Service

	// configuration
	configuration *Configuration

	// key manager
	keyManager *keystore.KeyManager

	// services library
	RemoteControllerLibrary *RemoteControllerLibrary

	// http client
	httpClient *http.Client

	// is running?
	isRunning bool

	// rabbit mq controller
	rabbitMQController *rabbit.Controller

	// sensor database handler
	database *Database

	// metrics
	metric *Metric
}

// this is the function you will use to build your service
func BuildService(listeningPort int,
	configuration *Configuration) (*Service, error) {
	metric := BuildMetric()
	service := &Service{
		configuration:           configuration,
		RemoteControllerLibrary: BuildRemoteControllerLibrary(metric),
		isRunning:               true,
		keyManager: keystore.BuildKeyManager(configuration.SecurityManager.Hostname,
			configuration.SecurityManager.Username,
			configuration.SecurityManager.Password,
			common.GhostService[common.ServiceDirectory].Name),
		httpClient: &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			},
		},
		metric: metric,
	}

	// build rabbit mq controller
	var err error
	if service.rabbitMQController, err = rabbit.BuildController(configuration.RabbitMQ.Hostname,
		configuration.RabbitMQ.Username,
		configuration.RabbitMQ.Password); err != nil {
		return nil, err
	}
	if channel := service.rabbitMQController.Channel; channel != nil {
		if _, err = channel.QueueDeclare(rabbit.ActionQueueName,
			true,
			false,
			false,
			false,
			nil); err != nil {
			return nil, err
		}
	} else {
		return nil, errors.New("can't get rabbit mq channel")
	}

	// build database
	if database, err := BuildDatabase(configuration,
		service.RemoteControllerLibrary,
		service.metric); err == nil {
		service.database = database
	} else {
		return nil, err
	}

	// build service with previous specified data
	service.service = common.BuildService(listeningPort,
		common.ServiceDirectory,
		DirectoryAPIService,
		VersionMajor,
		VersionMinor,
		nil,
		nil,
		configuration.SecurityManager.Hostname,
		service)

	// run updater
	go service.updateThread()

	// service is built
	return service, nil
}

// close
func (service *Service) Close() {
	service.isRunning = false
	service.rabbitMQController.Close()
}

// update thread
func (service *Service) updateThread() {
	for service.isRunning {
		// update controllers
		service.RemoteControllerLibrary.Update(service.keyManager,
			service.httpClient)

		// sleep
		time.Sleep(time.Millisecond * 16)
	}
}

// push new action event
func (service *Service) PushNewAction(action *orchestration.Action) error {
	// marshal action
	actionMarshal, _ := json.Marshal(action)

	// send to rabbit queue
	if channel := service.rabbitMQController.Channel; channel == nil {
		return errors.New("can't get channel")
	} else {
		if err := channel.Publish("",
			rabbit.ActionQueueName,
			false,
			false,
			amqp.Publishing{
				ContentType: "application/json",
				Body:        actionMarshal,
			}); err != nil {
			return err
		} else {
			return nil
		}
	}
}

// is running?
func (service *Service) IsRunning() bool {
	return service.isRunning
}
