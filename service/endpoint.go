package service

import (
	"encoding/json"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"io/ioutil"
	"net/http"
)

// define your services ids, must start from last built in index
const (
	// login based on an OTP
	// use to contact the service from outside of the
	// internal zone
	// delivers a session ID valid for a given period
	APIServiceV1DirectoryExternalLogin = common.APIServiceType(iota + common.APIServiceBuiltInLast)

	// a service notify itself to directory (classic One Time Token system)
	// internal zone request
	APIServiceV1NotifyService

	// request what kind of devices types were found
	APIServiceV1DirectoryRequestDeviceTypeList

	// list devices of a given type
	APIServiceV1DirectoryRequestListDeviceType

	// list controller for given name and type
	APIServiceV1DirectoryRequestController

	// get active devices
	APIServiceV1DirectoryRequestActiveDevice

	// test a device
	APIServiceV1DirectoryRequestDeviceTest

	// create an action
	APIServiceV1DirectoryRequestCreateAction
)

// declare how you want your endpoints to be accessed to
// Path is the split version of URL.Path by '/' character. So to express /api/v1/first, you'll put here
// ["api", "v1", "first"]
// Method is the HTTP method to use to access this endpoint
// Callback is a function of type common.EndpointFunction which will be called when an HTTP request triggers an endpoint
var DirectoryAPIService = map[common.APIServiceType]*common.APIEndpoint{
	// directory services
	APIServiceV1DirectoryExternalLogin: {
		Path:                    []string{"api", "v1", "directory", "login"},
		Method:                  "POST",
		Description:             "login to the directory with a given OTP token and an username/password (external access)",
		Callback:                CallbackAPIServiceV1DirectoryLogin,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1NotifyService: {
		Path:                    []string{"api", "v1", "directory", "notify"},
		Method:                  "POST",
		Description:             "an internal service can notify itself to the directory service with this endpoint",
		Callback:                CallbackAPIServiceV1DirectoryNotifyService,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1DirectoryRequestDeviceTypeList: {
		Path:                    []string{"api", "v1", "directory", "types"},
		Method:                  "GET",
		Description:             "list all known devices types",
		Callback:                CallbackAPIServiceV1DirectoryRequestDeviceTypeList,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1DirectoryRequestListDeviceType: {
		Path:                    []string{"api", "v1", "directory", "list"},
		Method:                  "GET",
		Description:             "list known devices name for a given type (GET type=X)",
		Callback:                CallbackAPIServiceV1DirectoryRequestListDeviceType,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1DirectoryRequestController: {
		Path:                    []string{"api", "v1", "directory", "find"},
		Method:                  "GET",
		Description:             "list known controllers for a given device type/name (GET type,name)",
		Callback:                CallbackAPIServiceV1DirectoryFindControllerForTypeName,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1DirectoryRequestActiveDevice: {
		Path:                    []string{"api", "v1", "directory", "active"},
		Method:                  "GET",
		Description:             "list all currently active devices (3s time frame)",
		Callback:                CallbackAPIServiceV1DirectoryGetActiveDevice,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1DirectoryRequestDeviceTest: {
		Path:                    []string{"api", "v1", "directory", "test"},
		Method:                  "PUT",
		Description:             "test a device by its name/type",
		Callback:                CallbackAPIServiceV1DirectoryDeviceTest,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1DirectoryRequestCreateAction: {
		Path:                    []string{"api", "v1", "directory", "action"},
		Method:                  "PUT",
		Description:             "create an action (json{type,name,identifier,[]field{name,value}})",
		Callback:                CallbackAPIServiceV1DirectoryCreateAction,
		IsMustProvideOneTimeKey: false,
	},
}

// external login with username/password + OTP
func CallbackAPIServiceV1DirectoryLogin(_ http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	_ interface{}) int {
	return http.StatusOK
}

// an internal service notify itself
func CallbackAPIServiceV1DirectoryNotifyService(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// check one time token
	if key, err := keystore.CheckOneTimeKey(request,
		srv.configuration.SecurityManager.Hostname); err == nil {
		// add
		if srv.RemoteControllerLibrary.AddNewController(key) {
			rw.WriteHeader(http.StatusOK)
			return http.StatusOK
		} else {
			rw.WriteHeader(http.StatusNotModified)
			return http.StatusNotModified
		}
	} else {
		rw.WriteHeader(http.StatusUnauthorized)
		return http.StatusUnauthorized
	}
}

// list existing devices types
func CallbackAPIServiceV1DirectoryRequestDeviceTypeList(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// extract type list
	deviceTypeList := srv.RemoteControllerLibrary.ExtractKnownDeviceTypeList()

	// format
	deviceTypeNameList := make([]string,
		0,
		1)
	for _, deviceType := range deviceTypeList {
		deviceTypeNameList = append(deviceTypeNameList,
			device.CapabilityTypeName[deviceType])
	}

	// marshal
	if data, err := json.Marshal(deviceTypeNameList); err == nil {
		_, _ = rw.Write(data)
	} else {
		rw.WriteHeader(http.StatusInternalServerError)
		return http.StatusInternalServerError
	}
	return http.StatusOK
}

// list existing controllers for a given device type
func CallbackAPIServiceV1DirectoryRequestListDeviceType(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// get service
	srv := handle.(*Service)

	// parse form
	if err := request.ParseForm(); err == nil {
		// get device type
		if typeList := request.Form["type"]; typeList != nil &&
			len(typeList) > 0 {
			// extract known types
			deviceList := srv.RemoteControllerLibrary.ExtractControllerTypeList(typeList[0])

			// marshal
			if data, err := json.Marshal(deviceList); err == nil {
				_, _ = rw.Write(data)
			} else {
				rw.WriteHeader(http.StatusInternalServerError)
				return http.StatusInternalServerError
			}
		}
	}
	return http.StatusOK
}

func CallbackAPIServiceV1DirectoryFindControllerForTypeName(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)

	// parse request
	if err := request.ParseForm(); err == nil {
		// find
		name := common.ExtractFormValue(request,
			"name")
		_type := common.ExtractFormValue(request,
			"type")

		if capability := device.FindCapabilityFromName(_type); capability == device.CapabilityTypes {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"empty device type\"}"))
		} else {
			content, _ := json.Marshal(srv.RemoteControllerLibrary.FindControllerTypeName(name,
				capability))
			rw.Header().Add("Content-Type",
				"application/json")
			_, _ = rw.Write(content)
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't parse form\"}"))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func CallbackAPIServiceV1DirectoryGetActiveDevice(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if srv.database.activeSensor == nil {
		_, _ = rw.Write([]byte("[ ]"))
	} else {
		body, _ := json.Marshal(srv.database.activeSensor)
		_, _ = rw.Write(body)
	}
	return http.StatusOK
}

func CallbackAPIServiceV1DirectoryDeviceTest(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"name")
		_type := common.ExtractFormValue(request,
			"type")
		if len(name) <= 0 ||
			len(_type) <= 0 {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"missing name or type\"}"))
			return http.StatusBadRequest
		} else {
			if capability := device.FindCapabilityFromName(_type); capability < device.CapabilityTypes {
				if err := srv.PushNewAction(&orchestration.Action{
					Basis: orchestration.Basis{
						Name:        "TestAction",
						Description: "This is a test action",
						UUID:        orchestration.GenerateRandomUUID(),
					},
					Content: []orchestration.ActionContent{
						{
							Type: _type,
							Action: []orchestration.ActionContentAtom{
								{
									Name:       name,
									Identifier: orchestration.ActionIdentifierTest,
								},
							},
						},
					},
				}); err == nil {
					return http.StatusOK
				} else {
					rw.WriteHeader(http.StatusInternalServerError)
					_, _ = rw.Write([]byte("{\"error\":\"couldn't push action to rabbit: " +
						err.Error() +
						"\"}"))
					return http.StatusInternalServerError
				}
			} else {
				rw.WriteHeader(http.StatusBadRequest)
				_, _ = rw.Write([]byte("{\"error\":\"incorrect type\"}"))
				return http.StatusBadRequest
			}
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't parse form: " +
			err.Error() +
			"\"}"))
		return http.StatusBadRequest
	}
}

func CallbackAPIServiceV1DirectoryCreateAction(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	body, _ := ioutil.ReadAll(request.Body)
	var action common.DirectoryAction
	if err := json.Unmarshal(body,
		&action); err == nil {
		newAction := orchestration.Action{
			Basis: orchestration.Basis{
				Name:        "directoryAction",
				UUID:        orchestration.GenerateRandomUUID(),
				Description: "a directory generated action",
			},
			Content: []orchestration.ActionContent{
				{
					Type: action.Type,
					Action: []orchestration.ActionContentAtom{
						{
							Name:       action.Name,
							FieldList:  action.FieldList,
							Identifier: action.Identifier,
						},
					},
				},
			},
		}
		if err := srv.PushNewAction(&newAction); err == nil {
			return http.StatusOK
		} else {
			rw.WriteHeader(http.StatusInternalServerError)
			_, _ = rw.Write([]byte("{\"error\":\"couldn't push action to rabbit: " +
				err.Error() +
				"\"}"))
			return http.StatusInternalServerError
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"can't parse json: " +
			err.Error() +
			"\"}"))
		return http.StatusBadRequest
	}
}
