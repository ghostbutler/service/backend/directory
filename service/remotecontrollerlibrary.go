package service

import (
	"errors"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"net/http"
	"sync"
	"time"
)

const (
	DelayBetweenControllerUpdate = time.Second * 2
	DelayBeforeControllerDead    = time.Second * 10
)

type RemoteControllerLibrary struct {
	// mutex
	sync.Mutex

	// library
	ControllerList map[string]*RemoteController

	// metrics handler
	metric *Metric
}

// build remote controller library
func BuildRemoteControllerLibrary(metric *Metric) *RemoteControllerLibrary {
	return &RemoteControllerLibrary{
		ControllerList: make(map[string]*RemoteController),
		metric:         metric,
	}
}

// add controller
// returns true if done, false if controller already exists
func (remoteControllerLibrary *RemoteControllerLibrary) AddNewController(key *keystore.PublicKeyExport) bool {
	// lock
	remoteControllerLibrary.Lock()
	defer remoteControllerLibrary.Unlock()

	// look for service
	if serviceType, err := common.FindServiceTypeFromName(key.ServiceName); err == nil {
		// build key
		serviceKey := common.GhostService[serviceType].Name +
			"." +
			key.ServiceIP

		// check
		if controller, ok := remoteControllerLibrary.ControllerList[serviceKey]; ok {
			// update controller last contact
			controller.lastContactTime = time.Now()

			// nothing was done
			return false
		} else {
			// add controller
			remoteControllerLibrary.ControllerList[serviceKey] = BuildRemoteController(key.ServiceIP,
				serviceType,
				serviceKey,
				remoteControllerLibrary.metric)

			// done
			return true
		}
	} else {
		return false
	}
}

// update controllers
func (remoteControllerLibrary *RemoteControllerLibrary) Update(keyManager *keystore.KeyManager,
	httpClient *http.Client) {
	// lock
	remoteControllerLibrary.Lock()
	defer remoteControllerLibrary.Unlock()

	// iterate controllers
	for controllerKey, controller := range remoteControllerLibrary.ControllerList {
		// controller needs an update?
		if time.Now().Sub(controller.lastUpdateTime) >= DelayBetweenControllerUpdate {
			// controller is dead?
			if time.Now().Sub(controller.lastContactTime) >= DelayBeforeControllerDead {
				// remove controller
				delete(remoteControllerLibrary.ControllerList,
					controllerKey)
			} else {
				// update controller
				controller.Update(keyManager,
					httpClient)
				controller.lastUpdateTime = time.Now()
			}
		}
	}

	// count
	deviceTypeList := remoteControllerLibrary.extractKnownDeviceTypeList()
	for _, deviceType := range deviceTypeList {
		remoteControllerLibrary.metric.KnownDeviceCount.WithLabelValues(device.CapabilityTypeName[deviceType]).Set(float64(len(remoteControllerLibrary.extractDeviceTypeList(deviceType))))
	}
	remoteControllerLibrary.metric.KnownControllerCount.WithLabelValues().Set(float64(len(remoteControllerLibrary.ControllerList)))
}

// extract known device types list
func (remoteControllerLibrary *RemoteControllerLibrary) extractKnownDeviceTypeList() []device.CapabilityType {
	// device types list
	deviceTypeList := make(map[device.CapabilityType]interface{})

	// iterate controllers
	for _, controller := range remoteControllerLibrary.ControllerList {
		for _, deviceCapability := range controller.CapabilityList {
			deviceTypeList[deviceCapability] = nil
		}
	}

	// extract types
	typeList := make([]device.CapabilityType,
		0,
		1)
	for key := range deviceTypeList {
		typeList = append(typeList,
			key)
	}

	// done
	return typeList
}

func (remoteControllerLibrary *RemoteControllerLibrary) ExtractKnownDeviceTypeList() []device.CapabilityType {
	remoteControllerLibrary.Lock()
	defer remoteControllerLibrary.Unlock()
	return remoteControllerLibrary.extractKnownDeviceTypeList()
}

func (remoteControllerLibrary *RemoteControllerLibrary) extractDeviceTypeList(deviceType device.CapabilityType) []string {
	// output
	output := make([]string,
		0,
		1)

	// iterate controllers
	for _, controller := range remoteControllerLibrary.ControllerList {
		for _, dvc := range controller.DeviceLinkCapabilityList[deviceType] {
			output = append(output,
				dvc.Name)
		}
	}

	// done
	return output
}

func (remoteControllerLibrary *RemoteControllerLibrary) ExtractDeviceTypeList(deviceType device.CapabilityType) []string {
	remoteControllerLibrary.Lock()
	defer remoteControllerLibrary.Unlock()
	return remoteControllerLibrary.extractDeviceTypeList(deviceType)
}

// extract controllers addresses for a given type
func (remoteControllerLibrary *RemoteControllerLibrary) ExtractControllerTypeList(deviceTypeName string) []string {
	// output
	output := make([]string,
		0,
		1)

	// look for device type
	if deviceType := device.FindCapabilityFromName(deviceTypeName); deviceType != device.CapabilityTypes {
		// lock
		remoteControllerLibrary.Lock()
		defer remoteControllerLibrary.Unlock()

		// iterate controllers
		for _, controller := range remoteControllerLibrary.ControllerList {
			for _, dvc := range controller.DeviceLinkCapabilityList[deviceType] {
				output = append(output,
					dvc.Name)
			}
		}
	}

	// done
	return output
}

// is a known device?
func (remoteControllerLibrary *RemoteControllerLibrary) IsKnownDevice(name string,
	deviceType device.CapabilityType) error {
	// list all devices for capability type
	list := remoteControllerLibrary.ExtractDeviceTypeList(deviceType)

	// check if we do have known outlets
	if len(list) <= 0 {
		return errors.New("{\"error\":\"no device of this type\"}")
	}

	// empty name means all devices
	if name == "" {
		return nil
	}

	// look for device
	for _, foundDeviceName := range list {
		if name == foundDeviceName {
			return nil
		}
	}
	return errors.New("{\"error\":\"no device of this type with this name\"}")
}

func (remoteControllerLibrary *RemoteControllerLibrary) FindControllerTypeName(name string,
	_type device.CapabilityType) []device.DeviceController {
	// allocate output
	output := make([]device.DeviceController,
		0,
		1)

	// lock
	remoteControllerLibrary.Lock()
	defer remoteControllerLibrary.Unlock()

	// look for type
	for _, controller := range remoteControllerLibrary.ControllerList {
		if controller.IsKnownCapability(_type) {
			if name == "" {
				output = append(output,
					device.DeviceController{
						Controller: device.DeviceControllerAddress{
							Hostname: controller.Address,
							Port:     common.GhostService[controller.ServiceType].DefaultPort,
						},
						Type: device.CapabilityTypeName[_type],
						Name: name,
					})
			} else {
				for capability, remoteControllerList := range controller.DeviceLinkCapabilityList {
					if capability == _type {
						for _, remoteController := range remoteControllerList {
							if remoteController.Name == name {
								output = append(output,
									device.DeviceController{
										Controller: device.DeviceControllerAddress{
											Hostname: controller.Address,
											Port:     common.GhostService[controller.ServiceType].DefaultPort,
										},
										Type: device.CapabilityTypeName[_type],
										Name: name,
									})
							}
						}
					}
				}
			}
		}
	}

	// done
	return output
}
